# hww - Hello World Web http server

Hww is a simple http server written in Go, that gathers a container CPU usage and offers it as end points for Prometheus that can be seen further in Grafana dashboard.
Whole system consists of three Docker containers, that can be started with single docker-compose.yml file:

```console
$ docker-compose up -d
```

Grafana dashboard will be  available at http://localhost:3000.

To spin up Kubernetes version, run from the kubernetes sub folder:
```console
kubectl apply -f .
````

To remove the application execute:
```console
kubectl delete -f .
````
or
```console
kubectl delete ns hww
````

To view the Grafana dashboard at http://localhost:3000 run:
```console
kubectl port-forward -n hww service/grafana 3000:3000
````

hww command line parameters and http responces:
```console
$ ./hww -h
Simple http server that exposes CPU usage to world
Usage of ./hww:
  -a string
    	Address to listen on for HTTP requests. (default "localhost")
  -p string
    	Port to listen on for HTTP requests. (default "8080")
  -r int
    	Interval for front page refreshes (0=disable) (default 30)
  -v	Log request to console

Application end points:
- /  main front page
- /varz  CPU usage
- /healthz  application health state
- /healthzInvert  inverts health state
```
Use cases

```console
$ curl -i localhost:8080/varz
user 4090834
nice 4611
system 1240178
idle 8136976
iowait 41943
irq 0
softirq 53145
steal 0
guest 0
guest_nice 0
load_avg1 1.14
load_avg5 0.90
load_avg15 0.95
program_uptime 39
health 1
ready 1

$ curl -i localhost:8080/healthz
ok

$ curl -i localhost:8080/healthzInvert
Health status changed from true to false

$ curl -i localhost:8080/healthz
error
```
