package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"log/slog"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

type CpuStats [10]int
type LoadAverage [3]string

const (
	urlRoot         = "/"
	urlVars         = "/varz"
	urlHealth       = "/healthz"
	urlHealthInvert = "/healthzInvert"
)

var endpointUrls = []string{urlVars, urlHealth, urlHealthInvert}
var listenPort = flag.String("p", "8080", "Port to listen on for HTTP requests.")
var listenAddress = flag.String("a", "localhost", "Address to listen on for HTTP requests.")
var refreshInterval = flag.Int("r", 30, "Interval for front page refreshes (0=disable)")
var isVerbose = flag.Bool("v", false, "Log request to console")

var cpuStats CpuStats
var startTime time.Time
var health = true
var isReady = false
var cpuStatsMutex sync.Mutex

func main() {

	log.SetFlags(log.Lshortfile | log.LstdFlags)
	startTime = time.Now()
	flag.Usage = func() {
		fmt.Printf("Simple http server that exposes CPU usage to world\n")
		fmt.Printf("Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Printf(`
Application end points:
- %s  main front page
- %s  CPU usage
- %s  application health state
- %s  inverts health state`,
			urlRoot, urlVars, urlHealth, urlHealthInvert)
	}
	flag.Parse()
	if *isVerbose {
		slog.SetLogLoggerLevel(slog.LevelDebug)
	}

	http.HandleFunc(urlHealth, func(writer http.ResponseWriter, request *http.Request) {
		slog.Debug(fmt.Sprintf("%v %v", request.RemoteAddr, request.URL.Path))
		header, str := http.StatusOK, "ok"
		if !health {
			header, str = http.StatusInternalServerError, "error"
		}
		writer.WriteHeader(header)
		if _, err := io.WriteString(writer, str); err != nil {
			log.Printf("Error writing response for %s: %v", urlHealth, err)
		}
	})

	http.HandleFunc(urlHealthInvert, func(writer http.ResponseWriter, request *http.Request) {
		slog.Debug(fmt.Sprintf("%v %v", request.RemoteAddr, request.URL.Path))
		health = !health
		str := fmt.Sprintf("Health status changed from %v to %v", !health, health)
		if _, err := io.WriteString(writer, str); err != nil {
			log.Printf("Error writing response for %s: %v", urlHealthInvert, err)
		}
	})

	http.HandleFunc(urlVars, func(writer http.ResponseWriter, request *http.Request) {
		slog.Debug(fmt.Sprintf("%v %v", request.RemoteAddr, request.URL.Path))
		runTime := time.Now().Unix() - startTime.Unix()
		writer.WriteHeader(http.StatusOK)

		cpuStats, err := getCpuStats()
		if err != nil {
			log.Printf("Error getting cpu stats: %v", err)
		}

		loadAverage, err := getLoadAverage()
		if err != nil {
			log.Printf("Error getting load average: %v", err)
		}

		str := fmt.Sprintf("user %d\nnice %d\nsystem %d\nidle %d\niowait %d\n"+
			"irq %d\nsoftirq %d\nsteal %d\nguest %d\nguest_nice %d\n"+
			"load_avg1 %s\nload_avg5 %s\nload_avg15 %s\n"+
			"program_uptime %d\nhealth %v\nready %v\n",
			cpuStats[0], cpuStats[1], cpuStats[2], cpuStats[3], cpuStats[4],
			cpuStats[5], cpuStats[6], cpuStats[7], cpuStats[8], cpuStats[9],
			loadAverage[0], loadAverage[1], loadAverage[2],
			runTime, Bool2Int(health), Bool2Int(isReady))
		if _, err := io.WriteString(writer, str); err != nil {
			log.Printf("Error writing response for %s: %v", urlVars, err)
		}
	})

	addressPort := *listenAddress + ":" + *listenPort
	server := &http.Server{Addr: addressPort}
	http.HandleFunc(urlRoot, PageIndex)

	c := make(chan os.Signal, 1)
	go func() {
		signal.Notify(c, os.Interrupt)
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		sig := <-c
		log.Printf("Got signal %v, exiting ...\n", sig)
		server.Shutdown(ctx)
		os.Exit(1)
	}()

	log.Printf("Listening on port %s", addressPort)
	isReady = true
	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		log.Fatalf("ListenAndServe() error: %v", err)
	}
}

func PageIndex(writer http.ResponseWriter, request *http.Request) {
	slog.Debug(fmt.Sprintf("%v %v", request.RemoteAddr, request.URL.Path))
	metaRefresh := ""
	pageTop := "<html><head>%v<title>Hello World by Go lang</title></head>" +
		"<body><h1>Hello World from Go!</h1><p>Available end points: "
	pageBottom := "</body></html>"
	if *refreshInterval > 0 {
		metaRefresh = "<meta http-equiv=refresh content=" + strconv.Itoa(*refreshInterval) + ">"
	}
	pageTop = fmt.Sprintf(pageTop, metaRefresh)

	urls := ""
	for _, url := range endpointUrls {
		urls = fmt.Sprintf("%v <a href=\"%v\">%v</a> ", urls, url, url)
	}
	commands := []string{"date", "uptime", "ps"}
	commandsOutput := ""
	for _, cmd := range commands {
		out, err := RunCommand(cmd)
		if err != nil {
			out = err.Error()
		}
		commandsOutput += "command <b>" + cmd + "</b> output<br>" + out + "<br>"
	}
	page := fmt.Sprintf("%v %v <pre>%v</pre> %v",
		pageTop,
		urls,
		commandsOutput,
		pageBottom)
	if _, err := io.WriteString(writer, page); err != nil {
		log.Printf("Error writing response for index page: %v", err)
	}
}

func RunCommand(command string) (string, error) {
	output, err := exec.Command(command).Output()
	if err != nil {
		health, isReady = false, false
		return "", fmt.Errorf("unable to execute command: %v", err)
	}
	return string(output), nil
}

func getCpuStats() (CpuStats, error) {
	procStat, err := os.ReadFile("/proc/stat")
	if err != nil {
		health, isReady = false, false
		return CpuStats{}, err
	}
	firstTotalsLine := strings.Split(string(procStat), "\n")[0]
	var cpuStats CpuStats

	reSpace := regexp.MustCompile(`\s+`)
	s := reSpace.ReplaceAllString(firstTotalsLine, " ")

	fields := strings.Split(s, " ")
	for i, v := range fields[1:11] {
		cpuStats[i], err = strconv.Atoi(v)
		if err != nil {
			return CpuStats{}, err
		}
	}
	return cpuStats, nil
}

func Bool2Int(b bool) int {
	if b {
		return 1
	}
	return 0
}

func getLoadAverage() (LoadAverage, error) {
	loadAverage, err := os.ReadFile("/proc/loadavg")
	if err != nil {
		health, isReady = false, false
		return LoadAverage{}, err
	}
	firstLine := strings.Split(string(loadAverage), "\n")[0]
	lineFields := strings.Split(firstLine, " ")
	loadAvg := LoadAverage{lineFields[0], lineFields[1], lineFields[2]}
	return loadAvg, nil
}
